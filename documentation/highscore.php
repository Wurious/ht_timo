<?php
/*
 *  Project: Ht_Timo
 *  File: highscore.php
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Created
 */

$madeIt = false;
$madeItPos = 0;


if (isset($_GET["submit_name"]) == true and isset($_GET["submit_score"]) == true ){
    $new_entry = $_GET["submit_name"]; $new_score = $_GET["submit_score"];
    $data = "./highscoredata.txt";
    $lines = file($data);
    $i = 0;

    /* See whether new_entry is good enough */
    while ($i < 5) {
        $parser = explode(",",$lines[$i]);
        $cmp = intval($parser[1]); /* get rid of \n */
        /*
           If submitted score is lower than or equal to
           the value of cmp we move on
        */
        if ($new_score > $cmp){
            $madeItPos = ($i+1);
            $madeIt = true;
            break;
        }
        $i++;
    }

    /* If new_entry was good enough */
    if ($madeIt == true){
        /* Rearrange list and write to CSV-file */
        $file = fopen("./highscoredata.txt", 'w');
        $i = 0;
        while ($i < 5){
            $parser = explode(",",$lines[$i]);
            if (($i+1) === $madeItPos){
                fwrite($file,$new_entry.",".$new_score."\n");
            }
            else {
                fwrite($file,$parser[0].",".$parser[1]);
            }
            $i++;
        }
        fclose($file);
    }
}

/*
    highscoredata.txt is CSV-file which contains 5 lines and 2 values per line
    format -> player name, score
    and lines are ordered so that player with highest score appears in first line and so on
*/
if ($_GET["list"] == "show"){
    $data = "./highscoredata.txt";
    $lines = file($data);
    $i = 0; $id = 1;

    echo "<link rel='stylesheet' href='./timostyle.css' type='text/css'>";
    echo "<img src='./TIMOCO.png' height='64' width='64'><img src='./TIMO.png' height='80' width='134'><BR><BR>";
    echo "<table border='0' width='240'>";
    echo "<tr>";
    echo "<td class='hiscore0' align='center' width='20'>#</td>";
    echo "<td class='hiscore0' align='center' width='100'>Pelaaja</td>";
    echo "<td class='hiscore0' align='center' width=''>Pisteet</td></tr>";


    while ($i < 5) {
        $parser = explode(",",$lines[$i]);
        echo "<tr><td class='hiscore" . $id . "' align='center' width='20'>" . ($i+1) . "</td>";
        echo "<td class='hiscore" . $id . "'  align='center' width='100'>" . $parser[0] . "</td>";
        echo "<td class='hiscore" . $id . "'  align='center' width=''>" . $parser[1] . "</td></tr>";
        if ($id == 1) { $id = 2; } else { $id = 1; }
        $i++;
    }
    echo "</table>";
}
if (isset($_GET["submit_name"]) == true and isset($_GET["submit_score"]) == true ){
    echo "<BR><BR><table border'0' width='240'><tr><td class='hiscore1' align='center'>";
    if ($madeIt == false){
        echo "Olemme pahoillamme, " . $new_entry . ", mutta saavutuksesi toimitusjohtajana on parhaimmistollemme riittämätön.";
    }
    else {
        echo "Onnittelumme, " . $new_entry . "! Olette yltäneet sijalle " . $madeItPos . " huimalla " . $new_score . " pisteen saavutuksella!";
    }
    echo "</td></tr></table>";
}
?>
