/*
 *  Project: Ht_Timo
 *  File: Storage.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Pertsan läppäri added :P for more testing
 *  Dec 15 2014     Ville Kauranen      Created
 */

package ht_timo;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptException;

/**
 * 
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class Storage {
    /* Singleton implementation start */
    private static Storage obj = new Storage();
    
    static public Storage getStorage() {
        return obj;
    }
    /* Singleton implementation end */
    
    public ArrayList<Item> item_array = new ArrayList();
    public ArrayList<Package> package_array = new ArrayList();
    
    private Storage() {
        try {
            /* Default items */
            item_array.add(new Item("XBOXONE PELIKONSOLI","40*50*60","5.20",true,100));
            item_array.add(new Item("Ming-vaasi","25*25*60","1.5",true,50));
            item_array.add(new Item("Shiny Charizard GEN-I pelikortti","0.05*10*4","0.01",false,100));
            item_array.add(new Item("Nokia 3310","3*10*2.5","0.15",false,9001));
            item_array.add(new Item("Pertsan läppäri","30*54*4","4.50",false,100));
        } 
        catch (ScriptException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public int createShippingId(){
        //int i = (int) (new Date().getTime()/1000);
        /* Shipping ID comes from Unix Timestamp in seconds */
        return (int) (new Date().getTime()/1000);
    }
}
