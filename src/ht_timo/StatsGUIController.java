/*
 *  Project: Ht_Timo
 *  File: StatsGUIController.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches
 *  Dec 18 2014     Ville Kauranen      Created
 */

package ht_timo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class StatsGUIController implements Initializable {
    @FXML
    private WebView fx_WebView_HighScore;
    @FXML
    private Button fx_Button_CloseWindow;
    @FXML
    private ListView<String> fx_ListView_StatsElements;
    @FXML
    private ListView<String> fx_ListView_StatsContent;
    @FXML
    private Button fx_Button_SubmitScore;
    @FXML
    private TextField fx_TextField_PlayerName;
    @FXML
    private Label fx_Label_PlayerNameInfo;
    @FXML
    private Label fx_Label_OnGoingDeliveries;
    @FXML
    private Button fx_Button_RefreshStats;
    @FXML
    private ComboBox<String> fx_ComboBox_Reports;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /* WebView */
        fx_WebView_HighScore.getEngine().load("http://www.vtvstudio.net/ville/olio_ohjelmointi/highscore?list=show");
        /* Initial stat refresh */
        ActionEvent e = new ActionEvent();
        refreshStats(e);
    }    

    @FXML
    private void closeWindow(ActionEvent event) {
        Stage stage = (Stage) fx_Button_CloseWindow.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void submitScore(ActionEvent event) {
        boolean allIsOK = true;
        /* Name must be provided in correct format */
        
        if (fx_TextField_PlayerName.getText() == null || fx_TextField_PlayerName.getText().isEmpty()){
            fx_Label_PlayerNameInfo.setText("Tyhjä nimi ei kelpaa!"); allIsOK = false;
        }
        else {
            if (fx_TextField_PlayerName.getText().length() < 3){
                fx_Label_PlayerNameInfo.setText("Liian lyhyt nimimerkki! (väh. 3 merkkiä)");
                allIsOK = false;
            }
            if (fx_TextField_PlayerName.getText().length() > 12){
                fx_Label_PlayerNameInfo.setText("Liian pitkä nimimerkki! (max 12 merkkiä)");
                allIsOK = false;
            }
        }
        if (allIsOK == true){
            fx_Label_PlayerNameInfo.setText(null);
            
            DataBuilder db = DataBuilder.getDataBuilder();

            fx_WebView_HighScore.getEngine().load(
                "http://www.vtvstudio.net/ville/olio_ohjelmointi/highscore?list=show"
                + "&submit_name=" + fx_TextField_PlayerName.getText()
                + "&submit_score=" + db.getScore() 
            );
            
            /* Reset all stats */
            db.resetStats();
            ActionEvent e = new ActionEvent();
            refreshStats(e);
        }      
    }

    @FXML
    private void refreshStats(ActionEvent event) {
        DataBuilder db = DataBuilder.getDataBuilder();
        
        fx_Label_OnGoingDeliveries.setText("Paketteja kuljetuksessa: " + db.getOngoingDeliverycount());
        fx_ListView_StatsElements.getItems().clear();
        fx_ListView_StatsContent.getItems().clear();
        fx_ComboBox_Reports.setValue(null);
        fx_ComboBox_Reports.getItems().clear(); 
        
        for (int i = 0; i < db.deliveryReport_array.size(); i++){
            fx_ComboBox_Reports.getItems().add(db.deliveryReport_array.get(i));
        }
        
        fx_ComboBox_Reports.setPromptText("Kuljetusraportit " + db.deliveryReport_array.size() + " raportti(a).");
        
        fx_ListView_StatsElements.getItems().add("Pisteet:");
        fx_ListView_StatsElements.getItems().add("Pisteitä per paketti:");
        fx_ListView_StatsElements.getItems().add("Paras tulos lähetetystä paketista:");
        fx_ListView_StatsElements.getItems().add("Lähetetyt paketit:");
        fx_ListView_StatsElements.getItems().add("Ehjänä saapuneet:");
        fx_ListView_StatsElements.getItems().add("Matkalle jääneet paketit:");
        
        fx_ListView_StatsElements.getItems().add("Pisimmälle lähetetty onnistunut paketti:");
        fx_ListView_StatsElements.getItems().add("Ja sen paketin sisältämä esine:");
        fx_ListView_StatsElements.getItems().add("Lyhimmälle lähetetty epäonnistunut paketti:");
        fx_ListView_StatsElements.getItems().add("Ja sen paketin sisältämä esine:");
        
        fx_ListView_StatsContent.getItems().add("  "+db.getScore());
        fx_ListView_StatsContent.getItems().add("~ "+db.getAverageScore());
        fx_ListView_StatsContent.getItems().add("  "+db.getBestScoreFromDelivery());
        fx_ListView_StatsContent.getItems().add("  "+db.getDeliverycount()+ " kpl");
        fx_ListView_StatsContent.getItems().add("  "+db.getNonDestroyedItemcount()+ " kpl");
        fx_ListView_StatsContent.getItems().add("  "+db.getLostDeliveryCount()+ " kpl");
        fx_ListView_StatsContent.getItems().add("  "+db.getFurthestDelivered()+ " km");
        fx_ListView_StatsContent.getItems().add("  "+db.getFurthestDeliveredItem());
        fx_ListView_StatsContent.getItems().add("  "+db.getShortestDelivered()+ " km");
        fx_ListView_StatsContent.getItems().add("  "+db.getShortestDeliveredItem());
    }

    @FXML
    private void reports(ActionEvent event) {
        /* only prompt text is supposed to be "selected" */
        fx_ComboBox_Reports.setValue(null);
    }
}
