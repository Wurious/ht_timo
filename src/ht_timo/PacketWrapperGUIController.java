/*
 *  Project: Ht_Timo
 *  File: TimoGUIController.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches 
 *  Dec 18 2014     Ville Kauranen      Minor changes here and there
 *  Dec 17 2014     Ville Kauranen      Basic functionality
 *  Dec 16 2014     Ville Kauranen      Created basic layout
 */


package ht_timo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.script.ScriptException;

/**
 * FXML Controller class
 *
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class PacketWrapperGUIController implements Initializable {
    @FXML
    private ComboBox<String> fx_ComboBox_ItemSelect;
    @FXML
    private Button fx_Button_CloseWindow;
    @FXML
    private TextField fx_TextField_ItemName;
    @FXML
    private TextField fx_TextField_ItemSize;
    @FXML
    private TextField fx_TextField_ItemWeight;
    @FXML
    private CheckBox fx_CheckBox_ItemIsFragile;
    @FXML
    private Button fx_Button_ItemCreate;
    @FXML
    private Slider fx_Slider_ItemConditionValue;
    @FXML
    private Label fx_Label_ItemConditionValue;
    @FXML
    private Button fx_Button_ShippingClassInfo;
    @FXML
    private Label fx_Label_ItemCreationInfo;
    @FXML
    private RadioButton fx_RadioButton_ShippingClass1;
    @FXML
    private RadioButton fx_RadioButton_ShippingClass2;
    @FXML
    private RadioButton fx_RadioButton_ShippingClass3;
    @FXML
    private ComboBox<String> fx_ComboBox_PointOfDeparture;
    @FXML
    private ComboBox<String> fx_ComboBox_From;
    @FXML
    private ComboBox<String> fx_ComboBox_Destination;
    @FXML
    private ComboBox<String> fx_ComboBox_To;
    @FXML
    private Button fx_Button_PacketCreate;
    @FXML
    private Label fx_Label_ShippingClassInfo;
    @FXML
    private Label fx_Label_PacketShippingInfo;
    @FXML
    private Stage fx_Stage_PacketShippingInfo;
    @FXML
    private Label fx_Label_PacketCreationInfo;
    @FXML
    private Label fx_Label_PacketCreationInfoAddendum;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        DataBuilder db = DataBuilder.getDataBuilder();
        Storage sto = Storage.getStorage();
        
        /* Fetch data to PointOfDeparture and Destination combo boxes from DataBuilder */
        for (int i = 0; i < db.city_array.size(); i++){
            fx_ComboBox_PointOfDeparture.getItems().add(db.city_array.get(i));
            fx_ComboBox_Destination.getItems().add(db.city_array.get(i));
        }  
        /* Fetch data to ItemSelect combo box from Storage */
        for (int i = 0; i < sto.item_array.size(); i++){
            fx_ComboBox_ItemSelect.getItems().add(sto.item_array.get(i).getName());
        }     
    }    

    @FXML
    private void itemSelect(ActionEvent event) {
        /* Good place as any to reset these labels 
           if user creates more packages
        */
        fx_Label_PacketCreationInfo.setText("");
        fx_Label_PacketCreationInfoAddendum.setText("");
    }

    @FXML
    private void itemConditionValue(MouseEvent event) {
        int i = (int) fx_Slider_ItemConditionValue.getValue();
        fx_Slider_ItemConditionValue.setValue(i);
        fx_Label_ItemConditionValue.setText("Esineen kestävyys: "+i);
    }

    @FXML
    private void closeWindow(ActionEvent event) {
        Stage stage = (Stage) fx_Button_CloseWindow.getScene().getWindow();
        /* Close window of shipping class information if open */
        if (fx_Stage_PacketShippingInfo != null){
            fx_Stage_PacketShippingInfo.close();
        }    
        stage.close();
    }

    @FXML
    private void itemCreate(ActionEvent event) {
        boolean allFieldsOK = true; 
        
        /* TextFields with null or empty Strings are not acceptable */
        if (fx_TextField_ItemName.getText() == null || fx_TextField_ItemName.getText().isEmpty()){
            allFieldsOK = false;
        }
        if (fx_TextField_ItemWeight.getText() == null || fx_TextField_ItemWeight.getText().isEmpty()){
            allFieldsOK = false;
        }
        if (fx_TextField_ItemSize.getText() == null || fx_TextField_ItemSize.getText().isEmpty()){
            allFieldsOK = false;
        }
        else {
            /*
                Other requirements for ItemSize (String)
                it must have two *-characters
                it must not have any other non numeric characters
                there must be numbers before and after a *-character
                those numbers must not be zero eg. not 0*12*42
            
                add this functionality later
            */
        }
        if (allFieldsOK == true){
            Storage sto = Storage.getStorage();    
            try {
                /* Add new custom item to Storages item_array */
                sto.item_array.add(new Item(
                    fx_TextField_ItemName.getText()
                    ,fx_TextField_ItemSize.getText()
                    ,fx_TextField_ItemWeight.getText()
                    ,fx_CheckBox_ItemIsFragile.isSelected()
                    ,(int) fx_Slider_ItemConditionValue.getValue()
                ));
                fx_Label_ItemCreationInfo.setTextFill(Color.web("#008000"));
                fx_Label_ItemCreationInfo.setText(fx_TextField_ItemName.getText() + " luotu!");
                /* Set all fields to their default values */
                fx_TextField_ItemName.setText(null);
                fx_TextField_ItemSize.setText(null);
                fx_TextField_ItemWeight.setText(null);
                fx_CheckBox_ItemIsFragile.setSelected(false);
                fx_Slider_ItemConditionValue.setValue(100);
                fx_Label_ItemConditionValue.setText("100");
            } 
            catch (ScriptException ex) {
                Logger.getLogger(PacketWrapperGUIController.class.getName()).log(Level.SEVERE, null, ex);
            }

            /* Clear and fetch data to ItemSelect from Storage */
            fx_ComboBox_ItemSelect.getItems().clear();
            for (int i = 0; i < sto.item_array.size(); i++){
                fx_ComboBox_ItemSelect.getItems().add(sto.item_array.get(i).getName());
            }   
            /* Set newly added item as selected item */
            fx_ComboBox_ItemSelect.setValue(sto.item_array.get(sto.item_array.size()-1).getName());
        }
        else {
            /* Notify user that there is problem with inputs */   
            fx_Label_ItemCreationInfo.setTextFill(Color.web("#ff0000"));
            fx_Label_ItemCreationInfo.setText("Tarkista syötteet!");
        }
    }

    @FXML
    private void shippingClassInfo(ActionEvent event) {
        if (fx_Stage_PacketShippingInfo != null){
            fx_Stage_PacketShippingInfo.close();
        }
        try {
            fx_Stage_PacketShippingInfo = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("ShippingClassInfoGUI.fxml"));
            Scene scene = new Scene(root);
            fx_Stage_PacketShippingInfo.setScene(scene);
            fx_Stage_PacketShippingInfo.show();
            fx_Stage_PacketShippingInfo.setTitle("Tietoa luokista");
            fx_Stage_PacketShippingInfo.getIcons().add(new Image("file://localhost/" + System.getProperty("user.dir") + "/TIMOCO.jpg"));
            
            fx_Stage_PacketShippingInfo.setMinWidth(fx_Stage_PacketShippingInfo.getWidth());
            fx_Stage_PacketShippingInfo.setMinHeight(fx_Stage_PacketShippingInfo.getHeight());
            
        } catch (IOException ex) {
            Logger.getLogger(TimoGUIController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void shippingClass1(ActionEvent event) {
        /* If selected unselect other radio buttons */
        if (fx_RadioButton_ShippingClass1.isSelected() == true){
            fx_RadioButton_ShippingClass2.setSelected(false);
            fx_RadioButton_ShippingClass3.setSelected(false);
        }
    }

    @FXML
    private void shippingClass2(ActionEvent event) {
        /* If selected unselect other radio buttons */
        if (fx_RadioButton_ShippingClass2.isSelected() == true){
            fx_RadioButton_ShippingClass1.setSelected(false);
            fx_RadioButton_ShippingClass3.setSelected(false);
        }
    }

    @FXML
    private void shippingClass3(ActionEvent event) {
        /* If selected unselect other radio buttons */
        if (fx_RadioButton_ShippingClass3.isSelected() == true){
            fx_RadioButton_ShippingClass1.setSelected(false);
            fx_RadioButton_ShippingClass2.setSelected(false);
        }
    }

    @FXML
    private void pointOfDeparture(ActionEvent event) {
        /* Load SmartPost data based on selected city 
           also clear all items from combo box 
           and set selected as null
           so previous selections from other cities 
           cannot be used 
        */
        DataBuilder db = DataBuilder.getDataBuilder();
        fx_ComboBox_From.getItems().clear();
        fx_ComboBox_From.setValue(null);
        for (int i = 0; i < db.smartpost_array.size(); i++){
            if (fx_ComboBox_PointOfDeparture.getValue() != null){
                if (fx_ComboBox_PointOfDeparture.getValue().equals(db.smartpost_array.get(i).getCity()) == true){
                    fx_ComboBox_From.getItems().add(db.smartpost_array.get(i).getPostoffice());
                }
            }    
        }  
    }

    @FXML
    private void destination(ActionEvent event) {
        /* Load SmartPost data based on selected city 
           also clear all items from combo box 
           and set selected as null
           so previous selections from other cities 
           cannot be used 
        */
        DataBuilder db = DataBuilder.getDataBuilder();
        fx_ComboBox_To.getItems().clear();
        fx_ComboBox_To.setValue(null);
        for (int i = 0; i < db.smartpost_array.size(); i++){
            if (fx_ComboBox_Destination.getValue() != null){
                if (fx_ComboBox_Destination.getValue().equals(db.smartpost_array.get(i).getCity()) == true){
                    fx_ComboBox_To.getItems().add(db.smartpost_array.get(i).getPostoffice());
                }
            }
        }  
    }

    @FXML
    private void to(ActionEvent event) {
        /* No functionality yet */
    }

    @FXML
    private void from(ActionEvent event) {
        /* No functionality yet */
    }

    @FXML
    private void packetCreate(ActionEvent event) { 
        Storage sto = Storage.getStorage();
        
        boolean allInfoIsProvided = true;
        int problem = 0; /* 
            1 = item undefined
            2 = Shipping class undefined
            3 = too big item selected with class 2
            4 = Point of departure undefined
            5 = Destination undefined
            6 = From undefined
            7 = To undefined
            8 = From and To are the same
            two problems cannot occur at the same time
            because others will not be tested if one of them occurs
        */
        fx_Label_ItemCreationInfo.setText("");
        fx_Label_ShippingClassInfo.setText("");
        fx_Label_PacketShippingInfo.setText("");
        
        /* Check that all neccessary information is provided */
        if (fx_ComboBox_ItemSelect.getValue() == null || fx_ComboBox_ItemSelect.getValue().isEmpty()){
            allInfoIsProvided = false; problem = 1;
        }
        if (allInfoIsProvided == true &&
            fx_RadioButton_ShippingClass1.isSelected() == false &&
            fx_RadioButton_ShippingClass2.isSelected() == false &&
            fx_RadioButton_ShippingClass3.isSelected() == false 
            ){
            allInfoIsProvided = false; problem = 2;
        }   
        if (allInfoIsProvided == true && fx_RadioButton_ShippingClass2.isSelected() == true){
            /* Check whether selected item is too large in size for 2. class*/
            
            for (int i = 0; i < sto.item_array.size(); i++){
                if (fx_ComboBox_ItemSelect.getValue().equals(sto.item_array.get(i).getName())){
                    /* Reference size is 40000 cubic centimeters */
                    if (sto.item_array.get(i).getSizeValue() > 40000){
                        allInfoIsProvided = false; problem = 3;
                        break;
                    }
                }
            }       
            
        }
        if (allInfoIsProvided == true && (fx_ComboBox_PointOfDeparture.getValue() == null || fx_ComboBox_PointOfDeparture.getValue().isEmpty())){
            allInfoIsProvided = false; problem = 4;
        }    
        if (allInfoIsProvided == true && (fx_ComboBox_Destination.getValue() == null || fx_ComboBox_Destination.getValue().isEmpty())){
            allInfoIsProvided = false; problem = 5;
        }    
        if (allInfoIsProvided == true && (fx_ComboBox_From.getValue() == null || fx_ComboBox_From.getValue().isEmpty())){
            allInfoIsProvided = false; problem = 6;
        }  
        if (allInfoIsProvided == true && (fx_ComboBox_To.getValue() == null || fx_ComboBox_To.getValue().isEmpty())){
            allInfoIsProvided = false; problem = 7;
        }  
        if (allInfoIsProvided == true && fx_ComboBox_From.getValue().equals(fx_ComboBox_To.getValue()) == true){
            allInfoIsProvided = false; problem = 8;
        }
        /* No problems occured */
        if (allInfoIsProvided == true){

            Package packet = wrapPacket();

            /* reset everything */
            fx_Label_ItemCreationInfo.setText("");
            fx_Label_ShippingClassInfo.setText("");
            fx_Label_PacketShippingInfo.setText("");
            fx_ComboBox_ItemSelect.setValue(null);
            fx_ComboBox_PointOfDeparture.setValue(null);
            fx_ComboBox_Destination.setValue(null);
            fx_ComboBox_From.getItems().clear(); fx_ComboBox_From.setValue(null);
            fx_ComboBox_To.getItems().clear(); fx_ComboBox_To.setValue(null);
            fx_RadioButton_ShippingClass1.setSelected(false);
            fx_RadioButton_ShippingClass2.setSelected(false);
            fx_RadioButton_ShippingClass3.setSelected(false);
            
            /* 
               Set these after reseting ItemSelect because onAction 
               it will clear these labels
            */
            fx_Label_PacketCreationInfo.setText(packet.getName() + " luotu!");
            fx_Label_PacketCreationInfoAddendum.setText("Sis. " + packet.getContent().getName());

        }
        else {
            /* Inform the user about the problem */
            if (problem == 1){ 
                fx_Label_ItemCreationInfo.setTextFill(Color.web("#ff0000"));
                fx_Label_ItemCreationInfo.setText("Valitse pakattava esine!");
            }
            if (problem == 2){ 
                fx_Label_ShippingClassInfo.setText("Valitse luokka!");
            }
            if (problem == 3){ 
                fx_Label_ShippingClassInfo.setText("Liian suuri paketti 2. luokalle!");
            }
            if (problem >= 4 && problem <= 7){ 
                fx_Label_PacketShippingInfo.setText("Tarkista syötteet!");
            }
            if (problem == 8){ 
                fx_Label_PacketShippingInfo.setText("Lähtö- ja päätepisteet samat!");
            }
        }
    }
    private Package wrapPacket() {
        
        DataBuilder db = DataBuilder.getDataBuilder();
        Storage sto = Storage.getStorage();
        SmartPost to = null, from = null;
        
        /* Create corresponding SmartPost objects by provided postoffice  */
        
        for (int i = 0; i < db.smartpost_array.size(); i++){
            if (db.smartpost_array.get(i).getPostoffice().equals(fx_ComboBox_To.getValue()) == true){
                to = new SmartPost(
                     db.smartpost_array.get(i).getCode()
                    ,db.smartpost_array.get(i).getCity()
                    ,db.smartpost_array.get(i).getAddress()
                    ,db.smartpost_array.get(i).getAvailability()
                    ,db.smartpost_array.get(i).getPostoffice()
                    ,db.smartpost_array.get(i).getGeoPointLatitude()
                    ,db.smartpost_array.get(i).getGeoPointLongitude()   
                );
                break;
            }
        } 
        for (int i = 0; i < db.smartpost_array.size(); i++){
            if (db.smartpost_array.get(i).getPostoffice().equals(fx_ComboBox_From.getValue()) == true){
                from = new SmartPost(
                     db.smartpost_array.get(i).getCode()
                    ,db.smartpost_array.get(i).getCity()
                    ,db.smartpost_array.get(i).getAddress()
                    ,db.smartpost_array.get(i).getAvailability()
                    ,db.smartpost_array.get(i).getPostoffice()
                    ,db.smartpost_array.get(i).getGeoPointLatitude()
                    ,db.smartpost_array.get(i).getGeoPointLongitude()   
                );
                break;
            }
        }
        
        /* Create item to be included */
        
        Item includedItem = null;
        
        for (int i = 0; i < sto.item_array.size(); i++){
            if (sto.item_array.get(i).getName().equals(fx_ComboBox_ItemSelect.getValue()) == true){
                try {
                    includedItem = new Item(
                         sto.item_array.get(i).getName()
                        ,sto.item_array.get(i).getSizeString()
                        ,Float.toString(sto.item_array.get(i).getWeight())
                        ,sto.item_array.get(i).checkIfFragile()
                        ,sto.item_array.get(i).getConditionAtStart()
                    );
                } 
                catch (ScriptException ex) {
                    Logger.getLogger(PacketWrapperGUIController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
        }
        
        /* Wrap the packet based on selected shipping class */
        
        Package packet = null;
        
        if (fx_RadioButton_ShippingClass1.isSelected() == true){       
            packet = new FirstClass(
                fx_ComboBox_Destination.getValue()
                ,fx_ComboBox_PointOfDeparture.getValue()
                ,to
                ,from
                ,includedItem
                ,sto.createShippingId()
                ,1
                ,10
            );   
        }   
        if (fx_RadioButton_ShippingClass2.isSelected() == true){       
            packet = new SecondClass(
                fx_ComboBox_Destination.getValue()
                ,fx_ComboBox_PointOfDeparture.getValue()
                ,to
                ,from
                ,includedItem
                ,sto.createShippingId()
                ,2
                ,3
            );
        } 
        if (fx_RadioButton_ShippingClass3.isSelected() == true){       
            packet = new ThirdClass(
                fx_ComboBox_Destination.getValue()
                ,fx_ComboBox_PointOfDeparture.getValue()
                ,to
                ,from
                ,includedItem
                ,sto.createShippingId()
                ,3
                ,5
            );   
        } 
        sto.package_array.add(packet);
        return packet;
    }
}
