/*
 *  Project: Ht_Timo
 *  File: SecondClass.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches
 *  Dec 18 2014     Ville Kauranen      added calculateDeliveryResults
 *  Dec 15 2014     Ville Kauranen      Created
 */

package ht_timo;

/**
 * 
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class SecondClass extends Package {
    /** 
     * @param arg0 Name of the city where SmartPost at variable 'to' is located
     * @param arg1 Name of the city where SmartPost at variable 'from' is located
     * @param arg2 SmartPost object where to the package is to be sent
     * @param arg3 SmartPost object from the package is to be sent
     * @param arg4 Item to be shipped 
     * @param arg5 Id of the iten to be shipped
     * @param arg6 shipping class
     * @param arg7 score multiplier
     */
    public SecondClass(String arg0, String arg1, SmartPost arg2, SmartPost arg3, Item arg4, int arg5, int arg6, int arg7) {
        super(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }
    
    @Override
    public void calculateDeliveryResults(double arg0){
        distance_travelled = arg0;
        /*
            max_range = undefined
            max_size = 40000 cubic centimeters (cannot be bigger than that constructor will not allow that in packetwrapperguicontroller 
            D Condition (non fragile) = -1 per 30 kilometers
            D Condition (fragile) = -2 per 30 kilometers
            Score_yeld: 3 * distance * S/W * condition / condition_at_start;
            S/W = Size / (Weight*5000)
        */
        
        /* set condition after delivery */
        if (content.checkIfFragile() == false){
            content.setCondition((int) ((distance_travelled/30)*-1) );
        }
        else {
            content.setCondition((int) ((distance_travelled/30)*-2) );
        }
        
        double sw = (double) size_value / (double) (weight*5000);
        double condition_ratio = (double) content.getCondition()/ (double) content.getConditionAtStart();
        
        if (condition_ratio == 0){
            report = "Paketti hajosi kokonaan kuljetuksessa.";
            lost = false; destroyed = true;
            score_yeld = 0;
        }
        else {
            report = "Paketti pääsi perille " + condition_ratio * 100 + "% ehjänä.";
            score_yeld = (int) (score_multiplier * distance_travelled * sw * condition_ratio);
            lost = false; destroyed = false;
        }
    } 
}
