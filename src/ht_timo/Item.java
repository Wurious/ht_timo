/*
 *  Project: Ht_Timo
 *  File: ThirdClass.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches
 *  Dec 17 2014     Ville Kauranen      Extra functionality added
 *  Dec 15 2014     Ville Kauranen      Created
 */

package ht_timo;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * 
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class Item {
    private String name;
    private float weight;
    private double size_value; /* in cubic centimeters */
    private String size_string;
    private boolean fragile;
    private int condition, condition_at_start;
    
    /** 
     * @param arg0 Name of the item
     * @param arg1 Size of the item (String) for instance -> '10.5*20.0*14.5'
     * @param arg2 Weight
     * @param arg3 Flag to indicate whether item is fragile or not
     * @param arg4 initial condition
     */
    public Item(String arg0, String arg1, String arg2, boolean arg3, int arg4) throws ScriptException{
        name = arg0; size_string = arg1; weight = Float.parseFloat(arg2);
        fragile = arg3; condition = arg4; condition_at_start = arg4;
        /* 
        Get size_value from size_string using Javascript engine
        
        Reference:
        http://stackoverflow.com/questions/3422673/evaluating-a-math-expression-given-in-string-form
        */
        ScriptEngineManager sem = new ScriptEngineManager();
        ScriptEngine conv = sem.getEngineByName("JavaScript");
        size_value = (double) conv.eval(arg1);
    }
    
    public String getName(){
        return name;
    }
    
    public float getWeight(){
        return weight;
    }
    
    public double getSizeValue(){
        return size_value;
    }
    
    public String getSizeString(){
        return size_string;
    }
    
    public void setCondition(int arg0){ /* arg0 is negative */
        if ((condition_at_start + arg0) > 0){ condition += arg0; }
        else { condition = 0; } /* Fully broken */
    }
    
    public int getConditionAtStart(){
        return condition_at_start;
    }
    
    public int getCondition(){
        return condition;
    }
    
    public boolean checkIfFragile(){
        return fragile;
    } 
}
