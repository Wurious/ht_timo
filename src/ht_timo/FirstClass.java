/*
 *  Project: Ht_Timo
 *  File: FistClass.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches
 *  Dec 18 2014     Ville Kauranen      added calculateDeliveryResults
 *  Dec 15 2014     Ville Kauranen      Created
 */

package ht_timo;

/**
 * 
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class FirstClass extends Package {
    /** 
     * @param arg0 Name of the city where SmartPost at variable 'to' is located
     * @param arg1 Name of the city where SmartPost at variable 'from' is located
     * @param arg2 SmartPost object where to the package is to be sent
     * @param arg3 SmartPost object from the package is to be sent
     * @param arg4 Item to be shipped 
     * @param arg5 Id of the iten to be shipped
     * @param arg6 shipping class
     * @param arg7 score multiplier
     */
    public FirstClass(String arg0, String arg1, SmartPost arg2, SmartPost arg3, Item arg4, int arg5, int arg6, int arg7) {
        super(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }
    
    @Override
    public void calculateDeliveryResults(double arg0){
        distance_travelled = arg0;
        /*
            max_range = 150 (will get lost beyond 150)
            max_size = None
            D Condition (non fragile) = -1 per 10 kilometers
            D Condition (fragile) = instant 0
            Score_yeld: 10 * distance * S/W * condition / condition_at_start;
            S/W = Size / (Weight*5000)
        */
        if (distance_travelled > 150){
            report = "Paketti katosi kuljetuksessa. (Yli 150km ja 1.lk)";
            lost = true; destroyed = true; /* because non destroyed deliveries are archived later
            and if you lose a delivery it should not be recognised as non destroyed (and delivered) */
            score_yeld = 0;
        }
        else {
            if (content.checkIfFragile() == true){
                report = "Paketti hajosi kokonaan kuljetuksessa. (Särkyvää ja 1.lk)";
                lost = false; destroyed = true;
                score_yeld = 0;
            }
            else {
                double sw = (double) size_value / (double) (weight*5000);
                /* set condition after delivery */
                content.setCondition((int) ((distance_travelled/10)*-1) );
                /* In this case (FirstClass) condition can never be 0 so no checks for that */
                
                
                double condition_ratio = (double) content.getCondition()/ (double) content.getConditionAtStart();
                report = "Paketti pääsi perille " + condition_ratio * 100 + "% ehjänä.";
                score_yeld = (int) (score_multiplier * distance_travelled * sw * condition_ratio);
                lost = false; destroyed = false;
            }
        }
    } 
}
