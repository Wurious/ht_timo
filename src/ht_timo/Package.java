/*
 *  Project: Ht_Timo
 *  File: Package.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches
 *  Dec 18 2014     Ville Kauranen      added calculateDeliveryResults
 *  Dec 17 2014     Ville Kauranen      Added extra functionality
 *  Dec 15 2014     Ville Kauranen      Created
 */

package ht_timo;

/**
 *
 * @author Ville Kauranen
 * @version 1.0 Dec 19 2014
 */

public abstract class Package {
    protected String name;
    protected int shipping_class; /* 1-3 */
    protected float weight;
    protected double size_value; /* cm³ / cm^3 / cubic centimeter */
    protected String size_string; /* For instance '10.5,20.0,14.5' */
    protected Item content; /* Item the package contains */
    protected int id; /* Shipping id */
    protected int score_multiplier;
    protected double distance_travelled;
    protected String report;
    protected int score_yeld;
    protected boolean destroyed;
    protected boolean lost;
    
    protected String destination; 
    protected String point_of_departure;
    protected SmartPost to;
    protected SmartPost from;
    
    /** 
     * @param arg0 Name of the city where SmartPost at variable 'to' is located
     * @param arg1 Name of the city where SmartPost at variable 'from' is located
     * @param arg2 SmartPost object where to the package is to be sent
     * @param arg3 SmartPost object from the package is to be sent
     * @param arg4 Item to be shipped 
     * @param arg5 Id of the iten to be shipped
     * @param arg6 shipping class
     * @param arg7 score multiplier 
     */
    public Package(String arg0, String arg1, SmartPost arg2, SmartPost arg3, Item arg4, int arg5, int arg6, int arg7){
        destination = arg0; point_of_departure = arg1;
        to = arg2; from = arg3;
        content = arg4; id = arg5;
        shipping_class = arg6; score_multiplier = arg7;
        
        name = "TIMO-paketti " + shipping_class + ".lk nro: " + id;
        
        /* Package 'inherits' attributes from its content */
        weight = content.getWeight();
        size_value = content.getSizeValue();
        size_string = content.getSizeString(); 
    }
    /* This method differs between Shipping Classes */
    public abstract void calculateDeliveryResults(double arg0);
    
    public boolean getLostStatus(){
        return lost;
    }
    
    public boolean getDestroyedStatus(){
        return destroyed;
    }
    
    public int getDeliveryScore(){
        return score_yeld;
    }
    
    public String getDeliveryReport(){
        return report;
    }
    
    public String getName(){
        return name;
    }

    public int getId(){
        return id;
    }
    
    public float getWeight(){
        return weight;
    }
    
    public double getSizeValue(){
        return size_value;
    }
    
    public String getSizeString(){
        return size_string;
    }
    
    public SmartPost getSmartPostTo(){
        return to;
    }
    
    public SmartPost getSmartPostFrom(){
        return from;
    }
    
    public int getShippingClass(){
        return shipping_class;
    }
    
    /* Method to interact with Item inside the package */
    public Item getContent(){
        return content;
    }
}
