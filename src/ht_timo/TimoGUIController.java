/*
 *  Project: Ht_Timo
 *  File: TimoGUIController.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches 
 *  Dec 16 2014     Ville Kauranen      Basic functionality
 *  Dec 15 2014     Ville Kauranen      Created basic layout
 */

package ht_timo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


/**
 * Main program GUI controller
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class TimoGUIController implements Initializable {
    
    @FXML
    private WebView fx_WebView;
    @FXML
    private Button fx_Button_showLocationsOnMap;
    @FXML
    private ComboBox<String> fx_ComboBox_CitySelect;
    @FXML
    private Button fx_Button_removePaths;
    @FXML
    private Button fx_Button_refreshWebView;
    @FXML
    private Button fx_Button_CreatePacket;
    @FXML
    private Button fx_refreshStorage;
    @FXML
    private Button fx_sendPacket;
    @FXML
    private ComboBox<String> fx_ComboBox_SelectPacket;
    @FXML
    private Button fx_Button_StatisticWindow;
    @FXML
    private ComboBox<String> fx_ComboBox_MarkerColor;
    @FXML
    private Label fx_Label_CitySelectInfo;
    @FXML
    private Label fx_Label_MarkerColorInfo;
    @FXML
    private Label fx_Label_SelectPacketInfo;
    private Stage fx_Stage_CreatePacket;
    private Stage fx_Stage_StatisticWindow;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /* Initiate WebView */
        fx_WebView.getEngine().load("file://localhost/" + System.getProperty("user.dir") + "/index.html");
        
        /* Here the simpleton objects of DataBuilder and Storage are created */
        DataBuilder db = DataBuilder.getDataBuilder();
        Storage sto = Storage.getStorage();
        
        /* Fetch data to CitySelect from DataBuilder */
        for (int i = 0; i < db.city_array.size(); i++){
            fx_ComboBox_CitySelect.getItems().add(db.city_array.get(i));
        }     
        
        /* Set marker color ComboBox */
        fx_ComboBox_MarkerColor.getItems().add("Punainen");
        fx_ComboBox_MarkerColor.getItems().add("Vihreä");
        fx_ComboBox_MarkerColor.getItems().add("Sininen");
        fx_ComboBox_MarkerColor.getItems().add("Keltainen");
        fx_ComboBox_MarkerColor.getItems().add("Oranssi");
        fx_ComboBox_MarkerColor.getItems().add("Purppura");
    }   

    @FXML
    private void showLocationsOnMap(ActionEvent event) {
        /* clear info labels */
        fx_Label_CitySelectInfo.setText("");
        fx_Label_MarkerColorInfo.setText("");
        
        /* Draw SmartPost locations based on selected city only if provided */
        if (fx_ComboBox_CitySelect.getValue() != null && fx_ComboBox_MarkerColor.getValue() != null){
            
            String location = fx_ComboBox_CitySelect.getValue();
            DataBuilder db = DataBuilder.getDataBuilder();
            
            String c = "";
            if (fx_ComboBox_MarkerColor.getValue().equals("Punainen") == true){ c = "red"; }
            if (fx_ComboBox_MarkerColor.getValue().equals("Vihreä") == true){ c = "green"; }
            if (fx_ComboBox_MarkerColor.getValue().equals("Sininen") == true){ c = "blue"; }
            if (fx_ComboBox_MarkerColor.getValue().equals("Keltainen") == true){ c = "yellow"; }
            if (fx_ComboBox_MarkerColor.getValue().equals("Oranssi") == true){ c = "orange"; }
            if (fx_ComboBox_MarkerColor.getValue().equals("Purppura") == true){ c = "purple"; }
            
            for (int i = 0; i < db.smartpost_array.size(); i++){
                if (location.equals(db.smartpost_array.get(i).getCity()) == true){
                    fx_WebView.getEngine().executeScript("document.goToLocation('"
                    + db.smartpost_array.get(i).getAddress() + ", "
                    + db.smartpost_array.get(i).getCode() + " "
                    + db.smartpost_array.get(i).getCity() + "','"
                    + "<img src=http://www.vtvstudio.net/ville/olio_ohjelmointi/TIMO.png height=60 width=100><BR><BR>"
                    + "Postinumero: " + db.smartpost_array.get(i).getCode() + "<BR><BR>"        
                    + db.smartpost_array.get(i).getPostoffice() + " , "
                    + db.smartpost_array.get(i).getAddress() + "<BR><BR> "
                    + "Avoinna: " + db.smartpost_array.get(i).getAvailability() + "','"+ c + "')"
                    );
                }
            }
        }
        else {
            /* Notify user what inputs are needed */
            if (fx_ComboBox_CitySelect.getValue() == null){
                fx_Label_CitySelectInfo.setText("Virhe!");
            }
            if (fx_ComboBox_MarkerColor.getValue() == null){
                fx_Label_MarkerColorInfo.setText("Virhe!");
            }
        }
    }

    @FXML
    private void removePaths(ActionEvent event) {
        fx_WebView.getEngine().executeScript("document.deletePaths()");
    }
    @FXML
    private void refreshWebView(ActionEvent event){
        fx_WebView.getEngine().load("file://localhost/" + System.getProperty("user.dir") + "/index.html");
        /* reset other map related stuff too */
        fx_ComboBox_CitySelect.setValue(null);
        fx_ComboBox_MarkerColor.setValue(null);
        fx_Label_CitySelectInfo.setText("");
        fx_Label_MarkerColorInfo.setText("");
    }  

    @FXML
    private void createPacket(ActionEvent event) {
        if (fx_Stage_CreatePacket != null){
            fx_Stage_CreatePacket.close();
        }
        try {
            fx_Stage_CreatePacket = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("PacketWrapperGUI.fxml"));
            Scene scene = new Scene(root);
            fx_Stage_CreatePacket.setScene(scene);
            fx_Stage_CreatePacket.show();
            fx_Stage_CreatePacket.setTitle("Luo uusi paketti");
            fx_Stage_CreatePacket.getIcons().add(new Image("file://localhost/" + System.getProperty("user.dir") + "/TIMOCO.jpg"));
            
            fx_Stage_CreatePacket.setMinWidth(fx_Stage_CreatePacket.getWidth());
            fx_Stage_CreatePacket.setMinHeight(fx_Stage_CreatePacket.getHeight());
            
        } catch (IOException ex) {
            Logger.getLogger(TimoGUIController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @FXML
    private void refreshStorage(ActionEvent event) {
        Storage sto = Storage.getStorage();
        
        /* Fetch data to CitySelect from DataBuilder */
        fx_ComboBox_SelectPacket.getItems().clear();
        
        for (int i = 0; i < sto.package_array.size(); i++){
            fx_ComboBox_SelectPacket.getItems().add(sto.package_array.get(i).getName());
        }     
    }

    @FXML
    private void sendPacket(ActionEvent event) {
        Storage sto = Storage.getStorage();
        DataBuilder db = DataBuilder.getDataBuilder();
        Package packet = null; int j = 0;
        fx_Label_SelectPacketInfo.setText("");
         
        /* 
            Get correct package object
            ComboBox has String elements that refer to the correct object
        */
        if (fx_ComboBox_SelectPacket.getValue() != null){
            for (int i = 0; i < sto.package_array.size(); i++){
                if (fx_ComboBox_SelectPacket.getValue().equals(sto.package_array.get(i).getName()) == true){
                    packet = sto.package_array.get(i);
                    break;
                }
                j++;
            }
        }
        /* 
            No package was found 
        */
        if (packet == null){
            fx_Label_SelectPacketInfo.setText("Virhe! Valitse paketti ensin!");
        }
        
        /* Draw package delivery route to WebView */
        if (packet != null){
            ArrayList<Float> geopoint_array = new ArrayList<>();
            geopoint_array.add(packet.getSmartPostFrom().getGeoPointLatitude());
            geopoint_array.add(packet.getSmartPostFrom().getGeoPointLongitude());
            geopoint_array.add(packet.getSmartPostTo().getGeoPointLatitude());
            geopoint_array.add(packet.getSmartPostTo().getGeoPointLongitude());
            
            String color = null; int delay = 0;
            
            if (packet.getShippingClass() == 1){ color = "'blue'"; delay = 10000; }
            if (packet.getShippingClass() == 2){ color = "'green'"; delay = 20000; }
            if (packet.getShippingClass() == 3){ color = "'red'"; delay = 30000; }
            
            double distanceTravelled = (double) fx_WebView.getEngine().executeScript("document.createPath("+ geopoint_array + ", " + color + ", "+packet.getShippingClass() + ")");
            sto.package_array.remove(j);

            /* reset package list automatically */
            ActionEvent e = new ActionEvent();
            refreshStorage(e);

            /* 
                Our delivery boy Timo starts a new thread 
                because we would like to have somekind of delay
                before the user gets to know how the delivery went out
                If we use Thread.sleep(n); here it will make this thread
                and everything associated with it wait for n milliseconds too 
                ... and we dont want that.

                Reference:
                http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html
            */     
            Thread thread = new Thread(new DeliveryTimo(packet, distanceTravelled, delay));
            thread.start();
        }
    }

    @FXML
    private void selectPacket(ActionEvent event) {
        /* No functionality yet */
    }

    @FXML
    private void statisticWindow(ActionEvent event) {
        if (fx_Stage_StatisticWindow != null){
            fx_Stage_StatisticWindow.close();
        }
        try {
            fx_Stage_StatisticWindow = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("StatsGUI.fxml"));
            Scene scene = new Scene(root);
            fx_Stage_StatisticWindow.setScene(scene);
            fx_Stage_StatisticWindow.show();
            fx_Stage_StatisticWindow.setTitle("Tilastoja nörteille");
            fx_Stage_StatisticWindow.getIcons().add(new Image("file://localhost/" + System.getProperty("user.dir") + "/TIMOCO.jpg"));
            
            fx_Stage_StatisticWindow.setMinWidth(fx_Stage_StatisticWindow.getWidth());
            fx_Stage_StatisticWindow.setMinHeight(fx_Stage_StatisticWindow.getHeight());
            
        } catch (IOException ex) {
            Logger.getLogger(TimoGUIController.class.getName()).log(Level.SEVERE, null, ex);
        
        }
    }

    @FXML
    private void markerColor(ActionEvent event) {
        /* No functionality yet */
    }
}
