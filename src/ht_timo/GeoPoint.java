/*
 *  Project: Ht_Timo
 *  File: GeoPoint.java
 *  Date            Author              Changes
 *  Dec 15 2014     Ville Kauranen      Created
 */

package ht_timo;

/**
 *
 * @version 1.0 Dec 15 2014
 * @author Ville
 */
public class GeoPoint {
    private float latitude;
    private float longitude;
    
    public GeoPoint(float arg0, float arg1){
        latitude = arg0; longitude = arg1;
    }
    public void setGeoPoint(float arg0, float arg1){
        latitude = arg0; longitude = arg1;
    }
    public float getLatitude(){
        return latitude;
    }
    public float getLongitude(){
        return longitude;
    }
}
