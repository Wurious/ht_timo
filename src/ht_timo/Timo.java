/*
 *  Project: Ht_Timo
 *  File: Timo.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Final check
 *  Dec 15 2014     Ville Kauranen      Created
 */

package ht_timo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * main class
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class Timo extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("TimoGUI.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("TIMO SmartPost-järjestelmä");
        stage.getIcons().add(new Image("file://localhost/" + System.getProperty("user.dir") + "/TIMOCO.jpg"));
        stage.setScene(scene);
        stage.show();
        
        stage.setMinWidth(stage.getWidth());
        stage.setMinHeight(stage.getHeight());
    }
    
     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
