/*
 *  Project: Ht_Timo
 *  File: ShippingClassInfoGUIController.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Added info
 *  Dec 18 2014     Ville Kauranen      Created
 */
package ht_timo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class ShippingClassInfoGUIController implements Initializable {
    @FXML
    private Button fx_Button_CloseWindow;
    @FXML
    private WebView fx_WebView_Info;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /* WebView */
        fx_WebView_Info.getEngine().load("http://www.vtvstudio.net/ville/olio_ohjelmointi/info.html");
    }    

    @FXML
    private void closeWindow(ActionEvent event) {
        Stage stage = (Stage) fx_Button_CloseWindow.getScene().getWindow();
        stage.close();
    }
    
}
