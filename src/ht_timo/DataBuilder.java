/*
 *  Project: Ht_Timo
 *  File: DataBuilder.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches
 *  Dec 18 2014     Ville Kauranen      Additional features
 *  Dec 15 2014     Ville Kauranen      Created
 */

package ht_timo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * 
 * Main beast of burden - handles almost all data 
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class DataBuilder {
    /* Singleton implementation start */
    private static DataBuilder obj = new DataBuilder();
    
    static public DataBuilder getDataBuilder() {
        return obj;
    }
    /* Singleton implementation end */
    
    public ArrayList<SmartPost> smartpost_array = new ArrayList();
    public ArrayList<String> city_array = new ArrayList();
    private ArrayList<String> temp_array = new ArrayList();
    private Document doc;
    
    
    /* Stats for nerds stuff */
    public ArrayList<String> deliveryReport_array = new ArrayList();
    private int score = 0;
    private int averageScore = 0;
    private int bestScoreFromDelivery = 0;
    private int ongoingDeliverycount = 0;
    private int deliverycount = 0;
    private int nonDestroyedItemcount = 0;
    private int lostDeliverycount = 0;
    private double furthestDelivered = 0;
    private String furthestDeliveredItem = "N/a";
    private double shortestDelivered = 0;
    private String shortestDeliveredItem = "N/a";
    

    private DataBuilder() {
        try {
            loadSmartPostData();
        } catch (IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    private void loadSmartPostData() throws IOException {
        
        URL url = null;
        try {
            url = new URL("http://smartpost.ee/fi_apt.xml");
        } 
        catch (MalformedURLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String content;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
            content = "";
            String line;
            while((line = br.readLine()) != null){
                content += line + "\n";
            }
        }
        
        city_array.clear();

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = (Document) dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            parseSmartPostData();
        }
        catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
    
    /** 
    * Form city_array from unique elements of temp_array
    */
    private void finalizeCityArray(){
        int i = 1; int j;
        boolean noDuplicates = false;
        
        if (temp_array.isEmpty() == false){
            /* First elements is always unique */
            city_array.add(temp_array.get(0));
            /* 
                If city_array already has element -> skip
                otherwise add new element
            */           

            while (i < temp_array.size()){
                noDuplicates = true;
                j = 0;
                while (j < city_array.size()){
                    /* 
                        After the first instance when an duplicate element
                        is found break and continue main loop 
                    */
                    if (temp_array.get(i).equals(city_array.get(j)) == true && noDuplicates == true){
                        noDuplicates = false;
                        break;
                    }
                    j++;
                }
                /*
                    If not a single duplicate found add element to arraylist
                */
                if (noDuplicates == true){
                    city_array.add(temp_array.get(i));
                }
                i++;
            }
            temp_array.clear();
        }
    }
    
    private void parseSmartPostData(){
        String code, city, address, availability, postoffice;
        float lat, lng;
    
        NodeList nodes = doc.getElementsByTagName("place");
        
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            code = e.getElementsByTagName("code").item(0).getTextContent();
            city = e.getElementsByTagName("city").item(0).getTextContent();
            address = e.getElementsByTagName("address").item(0).getTextContent();
            availability = e.getElementsByTagName("availability").item(0).getTextContent();
            postoffice = e.getElementsByTagName("postoffice").item(0).getTextContent();
            lat = Float.parseFloat(e.getElementsByTagName("lat").item(0).getTextContent());
            lng = Float.parseFloat(e.getElementsByTagName("lng").item(0).getTextContent());
            temp_array.add(city);
            smartpost_array.add(new SmartPost(code, city, address, availability, postoffice, lat, lng));
        }
        finalizeCityArray();
    }
    
    public void packageOnDelivery(){
        ongoingDeliverycount++;
        deliverycount++;
    }
    
    /** 
     * @param arg0 package id
     * @param arg1 distance travelled
     * @param arg2 from String
     * @param arg3 to String
     * @param arg4 condition customised string output
     * @param arg5 score yeld
     * @param arg6 cargo item name
     * @param arg7 lost or not
     * @param arg8 destroyed or not
     * @param arg9 size string
     * @param arg10 size double
     * @param arg11 weight float
     */
    public void packageDelivered(int arg0, double arg1, String arg2, String arg3, String arg4, int arg5, String arg6, boolean arg7, boolean arg8, String arg9, double arg10, float arg11){
        /* Stats for nerds */
        ongoingDeliverycount--;
        score += arg5;
        averageScore = (int) (score/deliverycount);
        
        if (arg5 > bestScoreFromDelivery){
            bestScoreFromDelivery = arg5;
        }
        
        if (arg7 == true){ lostDeliverycount++; }
        if (arg8 == false){ nonDestroyedItemcount++; }
        
        /* Shortest delivered that got destroyed */
        if (shortestDelivered == 0 && arg8 == true) { /* first time */
            shortestDelivered = arg1; 
            shortestDeliveredItem = arg6;
        }
        else {
            if (shortestDelivered > arg1 && arg8 == true){
                shortestDelivered = arg1;
                shortestDeliveredItem = arg6;
            }
        }    
        
        /* furthest delivered that did not get destroyed */
        if (furthestDelivered < arg1 && arg8 == false){
            furthestDelivered = arg1;
            furthestDeliveredItem = arg6;
        }
        /*
            Add new entry to deliveryReport_array
            for example:
            Package number: id - travelled xx km
            Size: xx*xx*xx (xxx cm^3) Weight: xx,x kg
            From: SmartPost, address, city, code
            To: SmartPost, address, city, code
            Package condition: - 
            Score: -      
        */
        deliveryReport_array.add(
              "Paketti nro: " + arg0 + " - Matkasi: " + arg1 + " kilometriä \n"
            + "Koko: " + arg9 + " (" + arg10 + " cm³) Paino: " + arg11 + " kg\n"
            + "Mistä: " + arg2 + "\n"
            + "Mihin: " + arg3 + "\n"
            + arg4 + "\n"
            + "Pisteet: " + arg5
        );
    }
    
    public void resetStats(){
        /* Resets all stats for nerds */
        deliveryReport_array.clear();
        score = 0;
        averageScore = 0;
        bestScoreFromDelivery = 0;
        ongoingDeliverycount = 0;
        deliverycount = 0;
        nonDestroyedItemcount = 0;
        lostDeliverycount = 0;
        furthestDelivered = 0;
        furthestDeliveredItem = "N/a";
        shortestDelivered = 0;
        shortestDeliveredItem = "N/a";
    }
    
    public int getDeliverycount(){
        return deliverycount;
    }
    
    public int getOngoingDeliverycount(){
        return ongoingDeliverycount;
    }
    
    public void addScore(double arg0){
        score += (int) arg0;
    }
    
    public int getScore(){
        return score;
    }
    
    public int getAverageScore(){
        return averageScore;
    }
    
    public int getBestScoreFromDelivery(){
        return bestScoreFromDelivery;
    }
    
    public double getShortestDelivered(){
        return shortestDelivered;
    }
    
    public String getShortestDeliveredItem(){
        return shortestDeliveredItem;
    }
    
    public double getFurthestDelivered(){
        return furthestDelivered;
    }
    
    public String getFurthestDeliveredItem(){
        return furthestDeliveredItem;
    }
    
    public int getNonDestroyedItemcount(){
        return nonDestroyedItemcount;
    }
    
    public int getLostDeliveryCount(){
        return lostDeliverycount;
    }
    
}
