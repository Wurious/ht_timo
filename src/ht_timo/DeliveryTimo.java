/*
 *  Project: Ht_Timo
 *  File: DeliveryTimo.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches
 *  Dec 18 2014     Ville Kauranen      Created
 */

package ht_timo;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class DeliveryTimo implements Runnable {
    /*
        Reference:
        http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html
    */
    private double travelDistance;
    private int delay;
    private Package cargo;
    
    /**
     *
     * @param arg0 Package that DeliveryTimo is delivering
     * @param arg1 Distance of the delivery
     * @param arg2 How long delivery takes in milliseconds (Timos are mighty fast)
     */
    public DeliveryTimo(Package arg0, double arg1, int arg2){
        cargo = arg0; travelDistance = arg1; delay = arg2;
        /* Packet goes on delivery */
        DataBuilder db = DataBuilder.getDataBuilder();
        db.packageOnDelivery();
    }
    
    @Override
    public void run(){
        try {
            /* wait for it ... */
            Thread.sleep(delay);
            deliveryDone();
        } 
        catch (InterruptedException ex) {
            Logger.getLogger(DeliveryTimo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void deliveryDone(){
        /* Packet gets delivered */
        DataBuilder db = DataBuilder.getDataBuilder();
        cargo.calculateDeliveryResults(travelDistance);
        db.packageDelivered(
             cargo.getId()
            ,travelDistance    
            ,cargo.getSmartPostFrom().getPostoffice() + " "
            + cargo.getSmartPostFrom().getAddress() + " "
            + cargo.getSmartPostFrom().getCity() + " "
            + cargo.getSmartPostFrom().getCode()
            ,cargo.getSmartPostTo().getPostoffice() + " "
            + cargo.getSmartPostTo().getAddress() + " "
            + cargo.getSmartPostTo().getCity() + " "
            + cargo.getSmartPostTo().getCode()
            ,cargo.getDeliveryReport()
            ,cargo.getDeliveryScore()
            ,cargo.getContent().getName()
            ,cargo.getLostStatus()
            ,cargo.getDestroyedStatus()
            ,cargo.getSizeString() 
            ,cargo.getSizeValue()
            ,cargo.getWeight()
        );    
        /* holy ... */
    }
}
