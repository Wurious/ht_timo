/*
 *  Project: Ht_Timo
 *  File: SmartPost.java
 *  Date            Author              Changes
 *  Dec 19 2014     Ville Kauranen      Finishing touches
 *  Dec 15 2014     Ville Kauranen      Created
 */

package ht_timo;

/**
 * 
 * @version 1.0 Dec 19 2014
 * @author Ville
 */
public class SmartPost {
    /* Default Construction (useless now but was used for testing)*/
    private String code = "53850";
    private String city = "LAPPEENRANTA";
    private String address = "Skinnarilankatu 34";
    private String availability = "ma-pe 10.30 - 15.00, la 12.00 - 13.00";
    private String postoffice = "Pakettiautomaatti, Kirjakauppa Aalef Lappeenranta";
    private GeoPoint geopoint = new GeoPoint(61.0543675f, 28.1832322f); 

    /** 
     * @param arg0 post code
     * @param arg1 city name
     * @param arg2 street address
     * @param arg3 availability
     * @param arg4 postoffice
     * @param arg5 latitude
     * @param arg6 longitude
     */
    public SmartPost(String arg0, String arg1, String arg2, String arg3, String arg4, float arg5, float arg6){
        code = arg0; city = arg1; address = arg2; availability = arg3;
        postoffice = arg4; 
        geopoint.setGeoPoint(arg5, arg6);
    }
    
    public String getCode(){
        return code;
    }
    
    public String getCity(){
        return city;
    }   
    
    public String getAddress(){
        return address;
    }
    
    public String getAvailability(){
        return availability;
    }
    
    public String getPostoffice(){
        return postoffice;
    }
    
    /* Access to geopoint */
    public float getGeoPointLatitude(){
        return geopoint.getLatitude();
    }
    
    public float getGeoPointLongitude(){
        return geopoint.getLongitude();
    }
}
